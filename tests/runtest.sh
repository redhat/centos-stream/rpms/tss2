#!/bin/bash


# get tpm simulator code
git clone https://git.code.sf.net/p/ibmswtpm2/tpm2 ibmswtpm2-tpm2

# unpackage and build the source
pushd ibmswtpm2-tpm2/src
# fixup for openssl 3
sed -i -e "s|OPENSSL_VERSION_NUMBER >= 0x10200000L|OPENSSL_VERSION_NUMBER > 0x30200020L|" TpmToOsslMath.h
sed -i -e "s|CCFLAGS = -Wall|CCFLAGS = -Wall -Wno-error=deprecated-declarations|" makefile
make

res="$?"

if [[ "$res" -ne 0 ]]; then
    echo "make of ibmtpm failed"
    popd
    rm -rf ibmtpm ibmtpm$IBMTPM_VERSION.tar.gz
    exit 1
fi

(./tpm_server)&
popd

sleep 10

export TPM_INTERFACE_TYPE=socsim

# use the tss2 tests from the sources
pushd ./source/utils
# fix python calls to use rhel name for python3
sed -i -e 's/^PREFIX=\.\//PREFIX=tss/g' reg.sh
# fix paths in rootcerts.txt
c=`pwd`
sed -i "s|/home/kgold/tss2/utils|${c}|" certificates/rootcerts.txt
# run the tests
TPM_TSS_NODEPRECATEDALGS=1 ./reg.sh -a
res="$?"
popd

# clean up
pkill tpm_server
rm -rf ibmtpm tss

exit $res
